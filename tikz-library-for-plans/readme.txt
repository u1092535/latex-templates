If you plan to draw AI plans -- examples are given in this folder and taken from the following
scientific papers:
- [Fig.1 & 2]  https://bercher.net/publications/2021/Bercher2021POCLComplexities.pdf
- [Fig.1 & 2]  https://bercher.net/publications/2020/Bercher2020POPvsPOCL.pdf
- [Fig.1, 5-7] https://bercher.net/publications/2019/Olz2019StepElimination.pdf
- [Fig.1]      https://bercher.net/publications/2024/Bercher2024PlanOptimizationSurvey.pdf

Then, consider using the tikz library "aiplans".

You find it online:
https://ctan.org/pkg/aiplans

Suggestions for improvement? Please use:
https://github.com/YikaiGe/tikz-aiplans/issues