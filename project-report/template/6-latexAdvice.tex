\chapter{Some Rules on how to use \LaTeX{} Correctly}\label{chap:LaTeXAdvice}

This chapter provides advice specifically on \LaTeX{}. Even if you are strong in \LaTeX{} already, you should carefully read this chapter because you might still make many errors that explained in here -- many of them are even still be done by actual scientists, so please don't skip this section. You should also use it as a checklist before you submit your work (or better yet: before you show it your supervisor(s)).


\section{Special Care with Dots that don't end Sentences}

You need to ``escape'' all blanks following a dot that does not end a sentence. E.g., ``This is 6 pt.\ project.''\ needs to be coded ``\verb!This is 6 pt.\ project.!'' as otherwise it looks as follows: ``This is 6 pt. project.''\ -- you see that in here the spacing after ``pt.''\ is way too large. This is because \LaTeX{} interprets each dot (with a following space) as one that ends a sentence -- after which more space is allocated. An escaped space in contrast produces a fixed space that doesn't get stretched. (Fun fact: when (mechanical) typewriters were still a thing, authors were hitting the space twice after each ``sentence-ending dot'' to produce exactly the behavior that \LaTeX{} does automatically.)

Interestingly, \LaTeX{} even adds this extra space if a dot is within an ending parentheses (like here.) I assume this is because in such cases one is not supposed to add another dot after the parenthesis to end the sentence, so it interprets this dot as ending the sentence although it's placed within a parenthesis. If this is not the case in your context, you will have to protect the space following the parenthesis, as in ``\verb!Figure, Table, Listing, Equation,! \verb!Chapter (etc.)\ references!'', as otherwise, as shown in ``Figure, Table, Listing, Equation, Chapter (etc.) references'', the space will be slightly too large.


\section{Use the right Dashes}

In \LaTeX, there are three kinds of dashes/hyphens:
\begin{itemize}
  \item - (in \LaTeX: \verb!-!), hyphen: This hyphen is \emph{only} used to concatenate words, e.g., when you write ``state-of-the-art approach''. (A spelling rule often done wrong is that we still have to write ``this approach is state of the art'', although it would be ``this is a state-of-the-art approach''.)
  \item -- (in \LaTeX: \verb!--!), en dash: This is a dash and usually used to depict ranges, e.g., instead to writing ``on pages 23 to 42'' we could write ``on pages 23--42''.
  \item --- (in \LaTeX: \verb!---!), em dash: This longer dash is usually used to set off some information. For example, I hope that adding this section leads towards no student using a hyphen where a dash would be required --- although I assume that many will still do this wrong. :(
\end{itemize}

The most important message here is that you should basically never use the hyphen where a dash is required. Whether you use the en dash or the em dash is however not of major importance as long as you stay consistent.


\section{Use the right Quotation Marks}

An error extremely often done in \LaTeX{}, is using the wrong quotation marks. These are ``correct quotation marks'', whereas these are "wrong ones". Just remember that correct quotation marks are always ``66/99'', whereas many students/\LaTeX{} beginners often incorrectly use "99/99". In \LaTeX{} code, this looks like:
\begin{itemize}
  \item 66/99, ``correct quotation marks''; keyboard symbols: \textasciigrave and \textquotesingle, so it's \textasciigrave\textasciigrave$\dots$\textquotesingle\textquotesingle
  \item 99/99, ''\textbf{wrong} pair of quotation marks''; \textquotesingle\textquotesingle$\dots$\textquotesingle\textquotesingle{} or \verb!"!$\dots$\verb!"! in the code.
  \item 66/66, ``another \textbf{wrong} pair of quotation marks``; \textasciigrave\textasciigrave$\dots$\textasciigrave\textasciigrave{} in the code.
\end{itemize} 
In fact, when somebody makes this error, it's most likely because they use the keyboard symbol \verb!"! both on the left and on the right, but on the left you need a different symbol, i.e., use \verb!`! twice! On the right, it does not matter whether you use the symbol \textquotesingle{} twice or the symbol \verb!"! once as both produce the same symbol in the PDF.



\section{Variables Names}

Very often, variable names will not be single letters, but \emph{words}, such as \emph{pre} for precondition or \emph{eff} for \emph{effects}. Since variables are often used in math mode, there's the temptation to just write them in math mode, e.g., \verb!$\langle pre,eff\rangle$!, resulting into ``$\langle pre,eff\rangle$''. You hopefully see that this looks incredibly ugly -- because \LaTeX{} sets the text incorrectly. Instead, you should put it into math italics. To save you effort, you should define a new macro:
\begin{center}
  \verb!\newcommand{\Pre} {\ensuremath{\mathit{pre}}}!\\
  \verb!\newcommand{\Eff} {\ensuremath{\mathit{eff}}}!
\end{center}
  
With this you can now simply write \verb!$\langle \Pre,\Eff\rangle$!, which now results into $\langle \Pre,\Eff\rangle$, which looks exactly as it should. (This template includes the file macros.tex, which you can use for all your macros.)



\section{Using Graphics/Plots/Tables/Pictures correctly}

% the figures are put in here so that it can be moved around in the document more easily
% (since this way it's one line to move, otherwise it might be a large block of code)
\input{6-exampleGraphics}

\begin{itemize}
  \item Graphics ``float''. \LaTeX{} decides where they should be placed, not you. You can of course still influence that a bit (via the arguments for the figure environment, cf.~\url{https://tex.stackexchange.com/questions/39017/how-to-influence-the-position-of-float-environments-like-figure-and-table-in-lat}), depending on where you put the source code that includes the graphics, but \LaTeX{} will have the final word on where \emph{exactly} it will appear. Still, please make sure that your graphics appear at reasonable places so that reading the document remains being a pleasure. That means that you will have to reference each graphic. Thus, the reader will take a look at a graphic (i.e., figure) exactly when you reference it in the text, not when it's ``being seen''. (This also means that graphics/figures that are not referenced could and should be deleted from your work.)
  \item In Figure~\ref{fig:graphicCaptionBelow} you see an example figure with its caption below -- which looks very ugly. Do that if the graphic is centered and wide enough. In contrast, Figure~\ref{fig:graphicCaptionAside} provides the caption next to the figure -- which in this case looks quite good since the graphic is portrait rather than landscape, i.e., now there is no lost space.
\end{itemize}



\section{Tables}

Standard \LaTeX{} tables don't look particularly pleasing. Thus, it's generally recommended to use the \verb!booktabs! package, which was designed to produce aesthetically pleasing tables. Table~\ref{tab:meatPrices} provides an example, taken from the official manual (slightly adapted). One of the most important rules: Do not use vertical lines. Note that the table caption appears on top. This is set on purpose to align with several publishers, who demand that captions for tables are \emph{above} tables, whereas those for figures (i.e., everything else: graphics, plots etc.)\ are \emph{below}.
  
\begin{table}[h!] % the "h" means "here", so using that places it at a nicer position
  \begin{tabular}{llr}
    \toprule
    \multicolumn{2}{c}{\textbf{Item}} \\
    \cmidrule(r){1-2}
    \textbf{Animal} & \textbf{Description} & \textbf{Price} (\$)\\
    \midrule
    Gnat            & per gram             & 13.65      \\
                    & each                 & 0.01       \\
    Gnu             & stuffed              & 92.50      \\
    Emu             & stuffed              & 33.33      \\
    Armadillo       & frozen               & 8.99       \\
    \bottomrule
  \end{tabular}
  \caption{This table lists prices for different kinds of animal meat.\label{tab:meatPrices}}
\end{table}




\section{Colored Links}

By default you will see that all hyperlinks (e.g., to figures like Figure \ref{fig:graphicCaptionAside}, citations like by \cite{Smith2021Wubalubadubdub}, etc.) are colored. Personally, I (the author of this template) find that easier to read in the PDF than the alternative. The alternative is that hyperlinks are indicated by colored boxes that surround them (where the text itself remains black). You can choose between the two by the setting the option \verb!colorlinks = true! or \verb!colorlinks = false! in the hyperref definitions (where \verb!true! colors the words, whereas \verb!false! produces the box). Note a major difference between the two: The box is an annotation, so it's not visible when printing. If the text itself is colored then that's an actual text color, so it will appear as you see it in the PDF also in the printout. You can of course also change the colors.




\section{Math environments}

Just a very few very short notes on math environments. Very short since this is not supposed to be a \LaTeX{course}! Please use google to find tutorials etc.\ if needed.

\begin{compactitem}
  \item Inline math like $\sum\limits_{i=1}^n i=\frac{1}{2}(n\cdot (n+1))$ can be set using \verb!$!\emph{math stuff}\verb!$!.
  \item To have something appear in its own new line and centered like the following:
  \[\sum\limits_i=1^n i=\frac{1}{2}(n\cdot (n+1))\]
  For this you have to use \verb!\[!\emph{math stuff}\verb!\]!. Note that \verb!$$!\emph{math stuff}\verb!$$! technically works as well, but this is actually \emph{wrong}! Just never use this syntax. If curious why (although it seems to work as well), just google it.
  \item If you want to show several equations or a sequence thereof, there are useful environments like ``align'' or ``align$^*$'' (where the latter suppresses equation numbers). Again, just google it! But here's one example:
  \begin{align}
    \sum\limits_{i=1}^n i &= 1+2+3+4+\dots+n\\
    \text{fibonacci series:}   &= 1\ 1\ 2\ 3\ 5\ 8 \dots
  \end{align}
  If your thesis is math-heavy, I strongly recommend to read through the \textsc{amsmath} package documentation or google related tutorials.
\end{compactitem}



\section{Algorithms}

The following is a short example code using the \verb!algorithm2e! package. There are other packages for this, but this is one of the most frequently used ones.

\begin{algorithm}[H]
  \DontPrintSemicolon  % Don't print semicolon
  \KwData{Your input data}
  \KwResult{Result of the algorithm}
  \SetKwFunction{FMain}{Main}
  \SetKwProg{Fn}{Function}{:}{}
  \Fn{\FMain{}}{
      \tcc{initialize variables}
      $sum \leftarrow 0$\; \label{alg:line:sumInit}
      \For{$i\leftarrow 1$ \KwTo $n$}{
          \tcc{process each element}
          $sum \leftarrow sum + i$\;
      }
      \KwRet $sum$\;
  }
  \caption{Example algorithm using algorithm2e \label{alg:exampleAlg}}
\end{algorithm}

When you look into the \LaTeX{} code of Algorithm \ref{alg:exampleAlg}, you see that you can define labels for individual lines. This has been done as example for Line \ref{alg:line:sumInit}. It is very important to always use dynamic references (i.e., labels), in this case also for line numbers. This is because your algorithm might still change, and then your numbers would get outdated.



\section{Further \LaTeX{} Issues or Questions?}

In case your document doesn't compile, check out the log file and search for ``error'', often that points towards the problem quickly. I also recommend the following sources:
\begin{itemize}
  \item If you are a \LaTeX{} beginner, you might also want to take a look at a well-known \LaTeX{} introduction \citep{Oetiker2021LatexIntroduction}, which in the current version -- according to \citeauthor{Oetiker2021LatexIntroduction} -- takes ``only'' a bit more than two hours to work through.
  \item chatGPT can analyze your code, error messages, and even write code. Try it!
  \item If that doesn't help, use \url{https://stackoverflow.com/}. 
\end{itemize}




%\ \\[2em]
\vfill
This concludes the section on \LaTeX{} advice. Much of the advice provided is made wrong even by experienced scientists. So please use this section also as checklist, during writing your thesis and before handing it in.

\textbf{If you have any advice on how it could be improved further,\\
please reach out to me!}\\[.5em]
--- Pascal --- \hfill pascal.bercher@anu.edu.au