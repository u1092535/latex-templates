\chapter{Some Information on Marking}\label{chap:Marking}

The information provided here is \emph{not} part of an official marking guide, and in particular not by the ANU. If there is some overlap -- which there hopefully is -- then it is ``coincidental''. In other words, the advice provided here is general advice on how your thesis can be improved and what often counts. This might be different between kinds of works (e.g., 6 pt vs.\ 12 pt research project vs.\ Honours thesis), different Universities, or even between program convenors! However, ``in spirit'', some of the following criteria might certainly play a role.

To re-emphasize:\\[.5em]
\emph{\textbf{The following list is far from comprehensive and is merely intended to get you started thinking about possible evaluation criteria. Please discuss your work and what is expected from you with your supervisor(s).}}

On top of this, for the students from the ANU reading this, I would like to be crystal clear that the following sections are by no means any sort of official criteria. They are literally just there to increase the chance that you write a better work, but they are not reflecting out official marking criteria.

\section{Write-up Quality}

There are several factors that play into account when judging the write-up. Here are some of them, though this list is clearly not exclusive.

\begin{itemize}
  \item \textbf{Claims backed up?} Every claim must be backed up by a citation, unless your claims are your own. So don't make statements that you cannot prove by appropriate citations. Most of the time, only $A^*$-ranked works should be cited, though there are exceptions to this rule. If in doubt, discuss with your supervisor(s). 
  
  \item \textbf{Citations new Enough?} You could check how ``old'' your citations are, to make sure you did not miss any new developments. If for example your newest citation is from more than 10 years ago, chances are high that you missed some relevant newer work -- and some reviewers might be looking for that. It clearly depends on your precise research question which works are related and hence when they were published. So in some cases, the newest relevant work might indeed be more than 10 years old, so in such a case there is in principle nothing wrong with the newest citation being ``older'' as it might still be the newest one research-wise. However, it might still make a bad impression so some reviewer checking for this, in particular if he/she is not an expert in the field of your thesis. So to be on the save side it might be worth checking how old your citations are and discuss with your supervisor(s) whether there are some works from recent years that can be appropriately cited. Keep in mind, chances are high that you could indeed cite some works from recent years as your research question is more likely to be a current one than to be a dated one that didn't attract attention in many years.
  
  \item \textbf{Results appropriately stated?} Never oversell your achievements. Objectively report on your findings, even if they are bad. Don't claim that your small work will revolutionize the world or some field -- that is very rarely true. Being humble is mostly likely closer to the truth -- and this is what science is all about.
  \item \textbf{Level of formality adequate?} In particular when it's about HD vs.\ D it becomes important how formally adequate a work is reported. For high HD it should be on a level that could be published in an $A^*$ venue, and the more informal/high-level everything is reported on, the worse for the mark.
  \item \textbf{Completeness and Self-Containedness.} Make sure that nothing important is missing. For example, not having a ``Related Work'' section isn't a good idea, but also check for completeness of the sections you have. For example, make sure your background section enables the reader to fully understand your work, without having to study any textbooks. For example, if your work builds on two subdisciplines of some field, providing background on only one of them would be a clear miss. Just make sure that your work is self-contained, i.e., that it can be understood with the material you provide.
  \item \textbf{General Form and Appearance.} This has many aspects, e.g., not having typos, not going over the border, using examples where helpful (graphically if possible), not having pages which are blank except a few lines, and many more. Just take care for the ``general appearance''.
  
  This is probably the least important point of all; but it still matters. Keep in mind that your reviewers don't have an intrinsic motivation to read your work -- they \emph{have} to. So make it \emph{pleasant} and easy for them!
\end{itemize}


\section{Factors Independent of Write-Up}

For some works the supervisor might be consulted for further input, mostly for ``scaling''. The input provided is often not visible in the work but counts never the less.

\begin{itemize}
  \item \textbf{Level of Independency.} It makes a huge difference whether the supervisor must explain every single bit, correct various parts multiple times to get all errors out, and even come up with the empirical design. Ideally, the supervisor only had to give the general topic/research idea and the student would do all the rest (and just ask for confirmation/feedback for the ideas he/she developed).
  \item \textbf{Amount of Contributions by the Student.} Whereas the main research question is always proposed by the supervisor, there is still often room for the student's won contributions, either on a technical level or more general regarding research ideas. Devising own ideas or approaches certainly improves the work/mark.
  \item \textbf{Level of Difficulty.} The harder the task or research question, the more valuable the work performed. Since the research question is given by the supervisor(s), the student barely has any influence on the hardness of the level of difficulty. However, if the work is conceptually easy (e.g., a simple application of existing techniques without any scientific or algorithmic challenges to be conquered by the student), then it becomes even more important that all the rest is of high quality, e.g., showing independency and high-quality write-up.
\end{itemize}
