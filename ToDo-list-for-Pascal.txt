# Open ToDos:

# presentation template

- presentation template (and possibly lectures): logo is outdated, the "l" has the wrong font (cf. report template, that's correct)


# report

- appendix: Add section that explains why there are blank pages (sometimes) before new sections.

- config: MAYBE allow two different ways of compiling the document: as book as it is right now, or as single PDF (without blank pages, no jumping borders etc.)

- appendix: The graphic explaining the size of borders is outdated in the sense that it uses
  a screenshot from the thesis which doesn't exist anymore in this form.